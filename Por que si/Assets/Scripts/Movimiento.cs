using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Movimiento : MonoBehaviour
{
    public SpriteRenderer render;
    public float runSpeed = 3;
    public float jumpSpeed = 3;
    private Animator animator;
    public static bool isGrouded;
    public CheckPoint spawn;
    Rigidbody2D rb;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
    }

    private void FixedUpdate()
    {
        if (Input.GetKey("d") || Input.GetKey("right"))
        {
            rb.velocity = new Vector2(runSpeed, rb.velocity.y);
            animator.SetBool("Caminar", false);
            render.flipX =false;

        }
        else if (Input.GetKey("a") || Input.GetKey("left"))
        {
            rb.velocity = new Vector2(-runSpeed, rb.velocity.y);
            animator.SetBool("Caminar", false);
            render.flipX = true;
        }
        else
        {
            rb.velocity = new Vector2(0, rb.velocity.y);
            animator.SetBool("Caminar", true);
        }


       if(Input.GetKey("space") && isGrouded)
       {
            rb.velocity = new Vector2(-rb.velocity.x, jumpSpeed);
       }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        isGrouded = true;

        if(collision.gameObject.tag == "ZonaMuerta")
        {
            transform.position = spawn.checkPoint;
        }
        if(collision.gameObject.tag == "CheckPoint")
        {
            spawn.GetComponent<CheckPoint>();
            spawn.gameObject.SetActive(false);
        }

    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        isGrouded = false;
    }
}
