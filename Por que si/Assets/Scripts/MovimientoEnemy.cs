using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovimientoEnemy : MonoBehaviour
{
    Rigidbody2D rb2D;
    [SerializeField] public float velocidadMovimiento;
    [SerializeField] public float distacia;
    [SerializeField] private LayerMask queEsSuelo;

    private void Start()
    {
        rb2D = GetComponent<Rigidbody2D>();
    }

    private void Update()
    {
        rb2D.velocity = new Vector2(velocidadMovimiento * transform.right.x, rb2D.velocity.y);

        RaycastHit2D infromationSuelo = Physics2D.Raycast(transform.position, transform.right, distacia, queEsSuelo);

        if(infromationSuelo)
        {
            Giro();
        }
    }

    private void Giro()
    {
        transform.eulerAngles = new Vector3(0, transform.eulerAngles.y + 180, 0);
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.right * distacia);
    }
}
